import logging

from pyjecteur.widget import MsgTypes, Widget

logging.basicConfig(level=logging.DEBUG)
logging.info("Starting")

w = Widget("/dev/ttyUSB0")

w.send_message(MsgTypes.RECEIVE_DMX_ON_CHANGE, b"\x00")

while True:
    m = w.read_message()
    if m[0] != MsgTypes.RECEIVED_DMX.value:
        logging.info("Not DMX")
        continue
    logging.info(f"DMX: {m[1][113:120]}")
